all: clean client server

test:
	@go test raft/rserver

clean:
	@rm -f server client

server:
	@go build -o server raft/rserver

client:
	@go build -o client raft/rclient
