# 2019-02-23 to 2019-02-28

Done
+ (feat) first round of voting passes
+ (feat) rough outline of AppendEntries
+ (bug) too many Elections by Leader
+ (bug) followers ElectionTimeout frequently
+ (bug) once a node is taken down, cannot break tie on vote
+ (test) take down Leader node and bring back up
+ (test) take down Follower node and bring back up
+ (test) take down Leader and bring back up quickly
+ (feat) create Client to accept from STDIN
+ (feat-7) convert to follower if see future term on any RPC request/response
+ (bug-5) first AppendEntries reply is false
+ (bug-6) Leader sending AppendEntries to itself
+ (bug-7) Followers error out on ReplyToAppendEntries
+ (test) add log entries!
+ (feat-8) leader recovery; if exists N such that N > commitIndex, a majority of matchIndex[i] >= N, and log[N].term == currentTerm, set commitIndex = N
+ (feat) implement RequestVote
+ (feat) implement AppendEntries
+ (feat) implement skeleton BeginElectionCycle
+ (feat-9) add endpoint as cmd line arg to client
+ (test-4) add test harness for server

Not Done
- (test-1) test election of new leader
- (test-2) test leader becomes follower
- (test-3) test on more than 3 nodes
- (feat-10) add granular lock to state
