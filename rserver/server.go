package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net"
	"net/rpc"
	"os"
	"strings"
	"time"
)

const N_TOTAL_SERVERS = 3

var serverRegistry = []string{
	"localhost:12345",
	"localhost:12346",
	"localhost:12347",
}

// ----------------------------------------------------

const HEARTBEAT_DURATION time.Duration = time.Second

type ServerType int

// represents the NIL vote, aka when
// a Follower starts up and has not
// yet participated in an Election
// cycle
const NIL_VOTE = "NIL"

// apparently how you do enums
const (
	Follower ServerType = iota
	Candidate
	Leader
)

type Entry struct {
	Index        int
	TermReceived int
	Command      string
}

type RequestVoteMessage struct {
	Term         int
	CandidateId  string
	LastLogIndex int
	LastLogTerm  int
}

type RequestVoteReply struct {
	Term        int
	VoteGranted bool
}

type AppendEntriesMessage struct {
	Term         int
	LeaderId     string
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []Entry
	LeaderCommit int
}

type AppendEntriesReply struct {
	Term    int
	Success bool
}

type AppendEntriesReplies [N_TOTAL_SERVERS - 1]AppendEntriesReply

type RequestVoteReplies [N_TOTAL_SERVERS - 1]RequestVoteReply

func (reply *RequestVoteReplies) isMajority(didVoteForSelf bool) bool {
	numVotesFor := 0
	if didVoteForSelf {
		numVotesFor += 1
	}

	// TODO make sure term is the same
	for _, entry := range reply {
		if entry.VoteGranted == true {
			numVotesFor += 1
		}
	}
	return int32(numVotesFor) > int32(N_TOTAL_SERVERS/2)
}

func min(x int, y int) int {
	if x < y {
		return x
	}
	return y
}

func generateRandomFloat32Between(lower float32, upper float32) float32 {
	return lower + rand.Float32()*upper
}

func getDurationBetweenOneToFiveSeconds() time.Duration {
	return time.Second * time.Duration(generateRandomFloat32Between(1.0, 5.0))
}

//******************************
// State Management / Transition
//******************************

type ServerState struct {
	// should stype and sid be metadata too?
	Stype ServerType
	SId   string

	// persistent
	CurrentTerm int
	VotedFor    string
	Log         []Entry

	// volatile
	CommitIndex      int
	LastAppliedIndex int

	// volatile state on leaders
	NextIndex  []int
	MatchIndex []int

	// metadata
	MetaElectionTimeout time.Time
	MetaEndpoint        string
	MetaTestMode        bool
}

func (s *ServerState) getLastLogIndex() int {
	if len(s.Log) == 0 {
		return 0
	}

	lastEntry := s.Log[len(s.Log)-1]
	return lastEntry.Index
}

func (s *ServerState) getLastLogTerm() int {
	if len(s.Log) == 0 {
		return 0
	}

	lastEntry := s.Log[len(s.Log)-1]
	return lastEntry.TermReceived
}

func (s *ServerState) hasLogEntryWithIndexAndTerm(term int, index int) (bool, bool) {
	if len(s.Log) == 0 {
		return true, true
	}
	hasIndex := len(s.Log) < index
	hasTerm := hasIndex && s.Log[index].TermReceived == term
	return hasTerm, hasIndex
}

// if server is no longer Candidate, this does a noop
func (s *ServerState) RequestVoteAsCandidate(m RequestVoteMessage) (RequestVoteReplies, error) {
	replies := RequestVoteReplies{}
	if s.Stype == Candidate {
		for i, entry := range serverRegistry {
			if entry == s.MetaEndpoint {
				continue
			}

			if !s.MetaTestMode {
				client, err := rpc.Dial("tcp", entry)
				if err != nil {
					return replies, err
				}

				// inner fn
				// TODO refactor everything outside into shared fn
				var reply RequestVoteReply

				if !s.MetaTestMode {
					err = client.Call("StateListener.RequestVote", m, &reply)
					if err != nil {
						return replies, err
					}
					log.Printf("[%s] RequestVoteReply from %v: %v\n", s.SId, entry, reply)
					replies[i] = reply
				}
			}
		}
		return replies, nil
	}
	return replies, nil
}

func (s *ServerState) SendAppendEntriesAsLeader() error {
	for i, ithServerIndex := range s.NextIndex {
		if ithServerIndex < s.CommitIndex {
			newEntries := make([]Entry, 0)
			for j := ithServerIndex; j < s.CommitIndex; j++ {
				newEntries = append(newEntries, s.Log[j])
			}
			var m = AppendEntriesMessage{
				Term:         s.CurrentTerm,
				LeaderId:     s.SId,
				PrevLogIndex: ithServerIndex,
				PrevLogTerm:  s.Log[ithServerIndex].TermReceived,
				Entries:      newEntries,
				LeaderCommit: s.CommitIndex,
			}

			endpoint := serverRegistry[i]
			if endpoint == s.MetaEndpoint {
				continue
			}
			if !s.MetaTestMode {
				client, err := rpc.Dial("tcp", endpoint)
				if err != nil {
					// return replies, err
					// TODO how to handle errors from Followers?
					log.Printf("[%s] while sending AppendEntries, could not connect to %s\n",
						s.SId, endpoint)
					continue
				}
				var reply AppendEntriesReply
				err = client.Call("StateListener.AppendEntries", m, &reply)
				if reply.Term > s.CurrentTerm {
					s.ConvertToFollowerOnFutureTerm(reply.Term)
				}
				if err != nil {
					// TODO how to handle errors from Followers?
					log.Printf("[%s] while sending AppendEntries, could not send message to %s\n",
						s.SId, endpoint)
					continue
				}
				log.Printf("[%s] succeeded in sending AppendEntries to %s. AppendEntriesReply=%v\n",
					s.SId, endpoint, reply)
			}
		}
	}

	return nil
}

func (s *ServerState) isMajorityMatchIndex(n int) bool {
	count := 0
	for _, index := range s.MatchIndex {
		if index >= n {
			count += 1
		}
	}
	return int32(count) > int32(len(s.MatchIndex)/2)
}

func (s *ServerState) CatchupCommitIndex() {
	for n := len(s.Log) - 1; n >= 0; n-- {
		if n > s.CommitIndex && s.isMajorityMatchIndex(n) && s.Log[n].TermReceived == s.CurrentTerm {
			s.CommitIndex = n
			break
		}
	}
}

func (s *ServerState) SendEmptyAppendEntriesAsLeader() (AppendEntriesReplies, error) {
	replies := AppendEntriesReplies{}
	if s.Stype == Leader {
		for i, entry := range serverRegistry {
			if entry == s.MetaEndpoint {
				continue
			}

			if !s.MetaTestMode {
				client, err := rpc.Dial("tcp", entry)
				if err != nil {
					return replies, err
				}

				// inner fn
				// TODO refactor everything outside into shared fn
				var m = AppendEntriesMessage{
					Term:         s.CurrentTerm,
					LeaderId:     s.SId,
					LeaderCommit: s.CommitIndex,
				}
				var reply AppendEntriesReply
				err = client.Call("StateListener.AppendEntries", m, &reply)
				if err != nil {
					return replies, err
				}
				log.Printf("[%s] AppendEntriesReply: %v\n", s.SId, reply)
				replies[i] = reply
				if reply.Term > s.CurrentTerm {
					s.ConvertToFollowerOnFutureTerm(reply.Term)
				}
			}
		}
		return replies, nil
	}
	return replies, nil
}

func (s *ServerState) SetUniqueEndpointAndId(endpoint string) {
	s.MetaEndpoint = endpoint
	s.SId = fmt.Sprintf("Node::%s", endpoint)
}

func (s *ServerState) ReplyToRequestVote(m RequestVoteMessage) RequestVoteReply {
	if m.Term > s.CurrentTerm {
		s.ConvertToFollowerOnFutureTerm(m.Term)
	}

	var YAY = RequestVoteReply{Term: s.CurrentTerm, VoteGranted: true}
	var NAY = RequestVoteReply{s.CurrentTerm, false}

	if m.Term < s.CurrentTerm {
		log.Printf("[%s] replying NAY to RequestVote from %s\n", s.SId, m.CandidateId)
		return NAY
	}

	if (s.VotedFor == NIL_VOTE || s.VotedFor == m.CandidateId) &&
		(m.LastLogTerm >= s.getLastLogTerm() &&
			m.LastLogIndex >= s.getLastLogIndex()) {
		if s.VotedFor == NIL_VOTE {
			s.VotedFor = m.CandidateId
		}
		// 5.2: on GrantVote, reset ElectionTimer
		s.ResetElectionTimer()
		log.Printf("[%s] replying YAY to RequestVote from %s\n", s.SId, m.CandidateId)
		return YAY
	}

	log.Printf("[%s] replying NAY to RequestVote from %s\n", s.SId, m.CandidateId)
	return NAY
}

func (s *ServerState) ConvertToFollowerOnFutureTerm(futureTerm int) {
	s.CurrentTerm = futureTerm
	s.Stype = Follower
}

func (s *ServerState) ReplyToAppendEntries(m AppendEntriesMessage) AppendEntriesReply {
	log.Printf("[%s] replying to AppendEntries from %s\n", s.SId, m.LeaderId)
	if m.Term > s.CurrentTerm {
		s.ConvertToFollowerOnFutureTerm(m.Term)
	}
	s.ResetElectionTimer()
	s.Stype = Follower

	if m.Term < s.CurrentTerm {
		log.Printf("[%s] NO to AppendEntries from %s; term older than current\n", s.SId, m.LeaderId)
		return AppendEntriesReply{
			Term:    s.CurrentTerm,
			Success: false,
		}
	}

	// replying to empty AppendEntries
	if len(m.Entries) == 0 {
		log.Printf("[%s] YES to Empty AppendEntries from %s;\n", s.SId, m.LeaderId)
		return AppendEntriesReply{
			Term:    s.CurrentTerm,
			Success: true,
		}
	}

	// response, but now have to take care of side effects
	hasTerm, hasIndex := s.hasLogEntryWithIndexAndTerm(
		m.PrevLogTerm, m.PrevLogIndex)

	// if index but not term, delete existing entry
	// and all that follow it
	if hasIndex && !hasTerm {
		log.Printf("[%s] index but not term; deleting existing entries after %d\n", s.SId, m.PrevLogIndex)
		s.Log = s.Log[m.PrevLogIndex:]
	}

	// append enw Entries
	yes := false
	for _, entry := range m.Entries {
		// log.Printf("[%s] len(s.Log)=%d entry.Index=%d\n", s.SId, len(s.Log), entry.Index)
		// logically, if len(s.Log) == entry.Index, it means we have
		// a new entry to append
		if len(s.Log) <= entry.Index {
			s.Log = append(s.Log, entry)
			yes = true
		}
	}

	if yes {
		log.Printf("[%s] appended new entries CommitIndex=%d Log=%v\n", s.SId, s.CommitIndex, s.Log)
	}

	// update leaderCommit
	if m.LeaderCommit > s.CommitIndex {
		indexOfLastNewEntry := s.Log[len(s.Log)-1].Index
		s.CommitIndex = min(m.LeaderCommit, indexOfLastNewEntry)
	}

	log.Printf("[%s] YES to AppendEntries from %s; CommitIndex=%d Log=%v\n", s.SId, m.LeaderId, s.CommitIndex, s.Log)

	return AppendEntriesReply{
		Term:    s.CurrentTerm,
		Success: hasTerm && hasIndex,
	}
}

func (s *ServerState) ReplyToReceiveCommand(command string) ClientCommandReply {
	if s.Stype != Leader {
		log.Printf("[%s] dropping command from client as it's not Leader\n", s.SId)
		return ClientCommandReply{Accepted: false}
	}

	entry := Entry{
		Index:        len(s.Log),
		TermReceived: s.CurrentTerm,
		Command:      command,
	}
	s.Log = append(s.Log, entry)

	// in this toy implementation, we commit as soon as we've written
	// to the log
	s.CommitIndex += 1
	s.LastAppliedIndex += 1
	log.Printf("[%s] Leader committed. CommitIndex=%d Log=%v\n", s.SId, s.CommitIndex, s.Log)
	return ClientCommandReply{Accepted: true}
}

func (s *ServerState) BeginElectionCycle() {
	s.CurrentTerm = s.CurrentTerm + 1
	s.Stype = Candidate
	s.VotedFor = s.SId
	s.ResetElectionTimer()

	rvMessage := RequestVoteMessage{
		Term:         s.CurrentTerm,
		CandidateId:  s.SId,
		LastLogIndex: s.getLastLogIndex(),
		LastLogTerm:  s.getLastLogTerm(),
	}

	replies, err := s.RequestVoteAsCandidate(rvMessage)
	if err != nil {
		log.Printf("[%s] error! %v\n", s.SId, err)
	}

	// if we get AppendEntries from existing or new leader,
	// convert to follower
	didVoteForSelf := s.VotedFor == s.SId
	if replies.isMajority(didVoteForSelf) {
		// it's possible that some rpc call snuck in and
		// updated the state. we need to always check before updating
		// stype
		//
		// if s is no longer a Candidate, this performs a no-op
		if s.Stype == Candidate {
			s.Stype = Leader
			s.VotedFor = NIL_VOTE
			log.Printf("[%s]: I just became leader!\n", s.SId)
			s.SendEmptyAppendEntriesAsLeader()
		}
	}
}

func (s *ServerState) ResetElectionTimer() {
	// set election timeout > 5x heartbeat
	// we want at least 5 heartbeats before
	// we begin a new election cycle, otherwise
	// servers will constantly start election
	// cycles
	s.MetaElectionTimeout = time.Now().Add(
		5*HEARTBEAT_DURATION +
			getDurationBetweenOneToFiveSeconds())
}

func (s *ServerState) SetDefaultServerStateAsFollower() {
	s.Stype = Follower
	s.VotedFor = NIL_VOTE
	s.ResetElectionTimer()
}

func (s *ServerState) InitEmptyState() {
	// if this node has never been the leader
	if len(s.Log) == 0 {
		s.Log = make([]Entry, 0)
		s.NextIndex = make([]int, N_TOTAL_SERVERS)
		s.MatchIndex = make([]int, N_TOTAL_SERVERS)
	}
	s.MetaTestMode = false
}

func (s *ServerState) HeartBeat() {
	switch s.Stype {
	case Follower:
		if time.Now().After(s.MetaElectionTimeout) {
			log.Printf("[%s] election timeout, converting to Candidate\n",
				s.SId)
			s.Stype = Candidate
		}
	case Leader:
		log.Printf("[%s] leader broadcasting empty AppendEntries\n",
			s.SId)
		// TODO Leaders.3(a,b)
		// TODO Leaders.4
		s.SendAppendEntriesAsLeader()
		s.SendEmptyAppendEntriesAsLeader()
		s.CatchupCommitIndex()
	case Candidate:
		log.Printf("[%s] election timeout, starting election cycle\n",
			s.SId)
		s.BeginElectionCycle()
	}
}

// *************
// RPC Interface
// *************

type StateListener struct {
	state *ServerState
}

type ClientCommandReply struct {
	Accepted bool
}

func (l *StateListener) RequestVote(m RequestVoteMessage, reply *RequestVoteReply) error {
	*reply = l.state.ReplyToRequestVote(m)
	return nil
}

func (l *StateListener) AppendEntries(m AppendEntriesMessage, reply *AppendEntriesReply) error {
	*reply = l.state.ReplyToAppendEntries(m)
	return nil
}

func (l *StateListener) ReceiveCommand(m string, reply *ClientCommandReply) error {
	*reply = l.state.ReplyToReceiveCommand(m)
	return nil
}

func main() {
	fEndpointP := flag.String("endpoint", "NIL_SERVER", "endpoint; one of `localhost:12345|12346|12347`")
	flag.Parse()
	fmt.Println(*fEndpointP)

	if *fEndpointP == "NIL_SERVER" {
		fmt.Println("Must specify endpoint")
		os.Exit(1)
	}

	// state for single server
	selfEndpoint := *fEndpointP

	var state = ServerState{}
	state.InitEmptyState()
	state.SetDefaultServerStateAsFollower()
	state.SetUniqueEndpointAndId(selfEndpoint)

	selfEndpointWithZeros := strings.Replace(selfEndpoint, "localhost", "0.0.0.0", 1)

	// wait few sec for user to boot up all servers
	log.Println("waiting 5s for other servers...")
	time.Sleep(5 * time.Millisecond)
	addr, err := net.ResolveTCPAddr("tcp", selfEndpointWithZeros)
	if err != nil {
		fmt.Println(err)
		log.Fatal("Couldn't connect to ", selfEndpointWithZeros)
	}

	inbound, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Accepting on ", selfEndpointWithZeros)
	var listener = StateListener{state: &state}
	rpc.Register(&listener)
	go rpc.Accept(inbound)

	fmt.Println("Starting HeartBeat...")
	// start HeartBeat
	for {
		state.HeartBeat()
		time.Sleep(HEARTBEAT_DURATION)
	}
}
