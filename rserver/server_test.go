package main

import (
	"testing"
	"time"
)

func TestInitEmptyState(t *testing.T) {
	s := ServerState{}
	s.InitEmptyState()
	if len(s.NextIndex) != N_TOTAL_SERVERS || len(s.MatchIndex) != N_TOTAL_SERVERS {
		t.Fail()
	}
}

func TestSetDefaultServerStateAsFollower(t *testing.T) {
	s := ServerState{}
	s.SetDefaultServerStateAsFollower()
	if s.Stype != Follower || s.VotedFor != NIL_VOTE {
		t.Fail()
	}

	if s.CurrentTerm != 0 {
		t.Fail()
	}
}

func TestResetElectionTimer(t *testing.T) {
	s := ServerState{}
	if (s.MetaElectionTimeout != time.Time{}) {
		t.Fail()
	}

	s.MetaElectionTimeout = time.Now()
	if (s.MetaElectionTimeout == time.Time{}) {
		t.Fail()
	}

	prevVal := s.MetaElectionTimeout
	s.ResetElectionTimer()
	if s.MetaElectionTimeout.Before(prevVal) {
		t.Fail()
	}
}

func TestBeginElection(t *testing.T) {
	s := ServerState{}
	s.MetaTestMode = true
	s.SetDefaultServerStateAsFollower()
	s.BeginElectionCycle()

	if s.CurrentTerm != 1 ||
		s.Stype != Candidate ||
		s.getLastLogIndex() != 0 ||
		s.getLastLogTerm() != 0 {
		t.Fail()
	}
}

func TestDefaultRequestVoteReply(t *testing.T) {
	reply := RequestVoteReply{}
	if reply.VoteGranted || reply.Term != 0 {
		t.Fail()
	}
}

func TestRepliesIsMajority(t *testing.T) {
	// test default state
	var didVoteForSelf bool
	var replies RequestVoteReplies

	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
	}
	didVoteForSelf = true
	if !replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
	}
	didVoteForSelf = false
	if replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	// test 2 yay
	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
		RequestVoteReply{1, true},
	}
	didVoteForSelf = true
	if !replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
		RequestVoteReply{1, true},
	}
	didVoteForSelf = false
	if !replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	// test 1 yay, 1 nay
	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
		RequestVoteReply{1, false},
	}
	didVoteForSelf = false
	if replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	// test 1 yay, 1 nay
	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
		RequestVoteReply{1, false},
	}
	didVoteForSelf = true
	if !replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	// test majority with different terms
	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
		RequestVoteReply{2, false},
	}
	didVoteForSelf = true
	if !replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	replies = RequestVoteReplies{
		RequestVoteReply{1, true},
		RequestVoteReply{2, false},
	}
	didVoteForSelf = true
	if !replies.isMajority(didVoteForSelf) {
		t.Fail()
	}

	replies = RequestVoteReplies{
		RequestVoteReply{1, false},
		RequestVoteReply{2, false},
	}
	didVoteForSelf = true
	if replies.isMajority(didVoteForSelf) {
		t.Fail()
	}
}

// ---------- First Term Tests --------------

func GetFirstTermServerState() (s ServerState) {
	entries := []Entry{
		Entry{0, 1, "push(1)"},
		Entry{1, 1, "push(2)"},
		Entry{2, 1, "push(3)"},
	}
	return ServerState{
		Stype:               Follower,
		SId:                 "Node1",
		CurrentTerm:         1,
		VotedFor:            "Node2",
		Log:                 entries,
		CommitIndex:         3,
		LastAppliedIndex:    3,
		MetaElectionTimeout: time.Now().Add(time.Millisecond * 5000),
		MetaEndpoint:        "localhost:12345",
		MetaTestMode:        true,
	}
}

func TestBeginElectionTermOne(t *testing.T) {
	s := GetFirstTermServerState()
	s.BeginElectionCycle()
	if s.CurrentTerm != 2 ||
		s.Stype != Candidate ||
		s.getLastLogIndex() != 2 ||
		s.getLastLogTerm() != 1 {
		t.Fail()
	}
}
