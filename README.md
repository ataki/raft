raft
----

yet another raft implementation. this is a first iteration, so discovering how
raft works in tandem with go.

I settled on an implementation with a "client" that mimics a data producer and
a "server" which implements Raft. In addition, I chose to make these command
line tools where the first argument is the port to bind to, for both server and
client.

Lastly, I left out membership and service discovery elements typical to a full
raft implementation. instead, servers assume that there will be 3 total
instances in the cluster started at the same time.

# Building

Running `make` creates a client and server binary in the project directory.

# Usage

First, start 3 instances of servers listening on different ports. these should
be executed in different terminal windows.

```
./server -endpoint="localhost:12345"
./server -endpoint="localhost:12346"
./server -endpoint="localhost:12347"
```

If this is done correctly, you should see the first node you start becoming
leader, and the rest becoming followers. each server should be actively
producing heartbeat logs.

Then, to start replicating, have the client connect to the leader. In this
case, say the server listening on localhost:12345 gets started first and
becomes the leader.

```
./client -endpoint="localhost:12345"
```

Then start entering text into STDOUT and see the logs of each server
updating to include the new text on all servers.
