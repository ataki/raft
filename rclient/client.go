package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/rpc"
	"os"
)

type ClientCommandReply struct {
	Accepted bool
}

func main() {
	fEndpointP := flag.String("endpoint", "NIL_SERVER", "endpoint to connec to; one of `localhost:12345|12346|12347`")
	flag.Parse()

	if *fEndpointP == "NIL_SERVER" {
		fmt.Println("Must specify endpoint")
		os.Exit(1)
	}

	selfEndpoint := *fEndpointP
	client, err := rpc.Dial("tcp", selfEndpoint)
	if err != nil {
		log.Fatal(err)
	}
	in := bufio.NewReader(os.Stdin)
	for {
		line, _, err := in.ReadLine()
		if err != nil {
			log.Fatal(err)
		}

		var reply ClientCommandReply
		err = client.Call("StateListener.ReceiveCommand", string(line), &reply)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Reply: %v", reply)
	}
}
